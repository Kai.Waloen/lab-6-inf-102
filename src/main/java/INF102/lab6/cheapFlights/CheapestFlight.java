package INF102.lab6.cheapFlights;

import java.util.*;

import INF102.lab6.graph.DirectedEdge;
import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {

    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> g = new WeightedDirectedGraph<>();

        for (Flight flight : flights) {
            City start = flight.start;
            City destination = flight.destination;
            int cost = flight.cost;

            g.addVertex(start);
            g.addVertex(destination);
            g.addEdge(start, destination, cost);
        }
        return g;
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {

    }

    class ReachInNStops {
        City destination;
        Integer nStops;

        public ReachInNStops(City destination, Integer nStops) {
            this.destination = destination;
            this.nStops = nStops;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this)
                return true;
            if (!(o instanceof ReachInNStops))
                return false;
            ReachInNStops other = (ReachInNStops) o;
            return this.destination.equals(other.destination) && this.nStops.equals(other.nStops);
        }

        @Override
        public int hashCode() {
            return Objects.hash(destination, nStops);
        }
    }

    class Trip implements Comparable<Trip>  {
        ReachInNStops destInNStops;
        Integer totalPrice;

        public Trip(ReachInNStops reach, Integer price) {
            this.destInNStops = reach;
            this.totalPrice = price;
        }

        @Override
        public int compareTo(Trip o) {
            return Integer.compare(totalPrice, o.totalPrice);
        }
    }
}

